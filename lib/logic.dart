


// ignore_for_file: curly_braces_in_flow_control_structures

class xoLogic{

  List<List<bool?>> _game = [];
  bool isX = true;

  xoLogic(){
    for(int i = 0 ; i < 3 ; i ++){
      _game.add([null,null,null]);
    }
  }

  bool? play(int i,int j){
    if(_game[i][j] == null) {
      _game[i][j] = isX;
      isX =          !isX;
      return _game[i][j];
    }
    return null;
  }

  void replay(){
    _game = [];
    for(int i = 0 ; i < 3 ; i ++){
      _game.add([null,null,null]);
    }
    isX = true;
  }

  String? check(){

  for(int i = 0 ; i < 3 ; i++ )
    {
      if(_game[i][0] != null && _game[i][0] == _game[i][1] && _game[i][2] == _game[i][1] )
      return (_game[i][0]!) ? 'X wins!' : 'O wins!';
    }

  for(int i = 0 ; i < 3 ; i++ )
  {
    if(_game[0][i] != null && _game[0][i] == _game[1][i] && _game[2][i] == _game[1][i] )
      return (_game[0][i]!) ? 'X wins!' : 'O wins!';
  }

  if(_game[0][0] != null && _game[0][0] == _game[1][1] && _game[2][2] == _game[1][1] )
    return (_game[0][0]!) ? 'X wins!' : 'O wins!';


  if(_game[0][2] != null && _game[0][2] == _game[1][1] && _game[2][0] == _game[1][1] )
    return (_game[0][2]!) ? 'X wins!' : 'O wins!';

  for(int i = 0 ; i < _game.length ; i++)
    for(int j = 0 ; j < _game[j].length ; j++)
      if(_game[i][j] == null)
        return null;

  return 'Draw no winner!!';

  }

}