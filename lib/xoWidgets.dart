
// ignore_for_file: file_names, prefer_final_fields, prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'logic.dart';

final xoLogic logic = xoLogic();


class XOWidget extends StatefulWidget {
  final int i;
  final int j;
  String _value = '';
  XOWidget(this.i, this.j);

  @override
  State<XOWidget> createState() => _XOWidgetState();
}

class _XOWidgetState extends State<XOWidget> {

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: GestureDetector(
          child: Container(
            width: double.infinity,
            height: double.infinity,
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10),
            ),
            margin: EdgeInsets.all(5),
            child: Center(
              child: Text(widget._value),
            ),
          ),
          onTap: (){

            String? result = logic.check();
            if(result != null)
              return;
            bool? isX = logic.play(widget.i, widget.j);
            result = logic.check();
            if( isX == null )
              return;
            setState(
                    () {
              widget._value = (isX!) ? 'X' : 'O';
              print(widget._value);
            }
            );
            if( result != null ){
              Alert(
                context:context,
                title: 'Game Over!!',
                desc: result!,
                buttons: []
              ).show();
            }
          },

        ),
      );

  }
}



class XORow extends StatelessWidget {

  final int i;

  List<Widget> _children = [];


  XORow(this.i);

  @override
  Widget build(BuildContext context) {

    for(int i = 0 ; i < 3 ; i ++){
      _children.add( XOWidget(this.i,i) );
    }

    return Expanded(
      flex: 3,
      child: Row(
        children: _children,
      ),
    );
  }
}



class XOBody extends StatefulWidget {
  List<Widget> _children = [];
  @override
  State<XOBody> createState() => _XOBodyState();
}

class _XOBodyState extends State<XOBody> {


  @override
  Widget build(BuildContext context) {

    for(int i = 0 ; i < 3 ; i ++){
      widget._children.add( XORow(i) );
    }

    widget._children.add(
      Expanded(
        flex: 2,
        child: Center(
          child: IconButton(
            icon: Icon(Icons.replay,size: 40,),
            onPressed: (){
              setState((){
                widget._children = [];
                logic.replay();
              });
            },
          ),
        ),
      )
    );

    return Column(
      children: widget._children,
    );
  }
}
